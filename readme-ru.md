# reactstrap-alert
[In English](https://gitlab.com/WhiterBlack/reactstrap-alert/blob/master/readme.md)

Простой способ для отображения диалогов при помощи reactstrap

Задача пакета заменить браузерные методы alert(), confirm(), prompt() на модальные окна в стиле bootstrap при помощи reactstrap

## Установка
* NPM
```bash
npm i --save reactstrap-alert
```
* yarn
```bash
yarn add reactstrap-alert
```
## Зависимости
* react
* reactsctrap
* lodash

# Использование

Просто импортируйте модуль

```javascript
import { Alert, Confirm } from 'reactstrap-alert'

// ...code

const answer = await Confirm("Вы действительно хотите удалить данного пользователя");
if(answer) {
    // ...code
    await Alert("Пользователь удалён")
}
```
Так же можно передавать объект вместо строки
### Параметры объекта

| Поле             | Описание                                                                                                     
| ---------------- | -------------------------------------------------------------------------------------------------------------
| message          | Текст модали                                                                                                 
| title            | Заголовок модали                                                                                             
| confirmText      | Текст на кнопке продолжить (Если не указан то кнопка отсутсвует)                                             
| cancelText       | Текст на кнопке отмена (Аналогично confirmText)                                                                        
| confirmColor     | Цвет кнопки продолжить (см. документацию [reactsctrap](https://reactstrap.github.io/components/buttons/#app))                                            
| cancelColor      | Цвет кнопки отмены (см. документацию [reactsctrap](https://reactstrap.github.io/components/buttons/#app))                                                   
