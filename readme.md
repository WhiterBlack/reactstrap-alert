# reactstrap-alert
[По русски](https://gitlab.com/WhiterBlack/reactstrap-alert/blob/master/readme-ru.md)

A simple way to display dialogs with reactstrap

The task of the package is to replace the browser methods alert(), confirm(), prompt() with modal windows in the style of bootstrap using reactstrap

## Installation
* NPM

```bash
npm i --save reactstrap-alert
````
* yarn

```bash
yarn add reactstrap-alert
````
## Dependencies
* react
* reactstrap
* lodash

# Usage

Just import the module

```javascript
import {Alert, Confirm} from 'reactstrap-alert'

// ... code

const answer = await Confirm ("Do you really want to delete this user");
if (answer) {
    // ... code
    await Alert ("User deleted")
}
````
