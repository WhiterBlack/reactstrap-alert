import React, {useState} from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

export function BaseModal(props) {
	const [isOpen, _toggle] = useState(true);

	function onResolve(value = false) {
		_toggle(false);
		props.resolve(value);
	}

	return (
		<Modal isOpen={isOpen} toggle={() => onResolve(false)} {...(props.modalProps || {})}>
			<ModalHeader toggle={() => onResolve(false)} {...(props.modalHeaderProps || {})}>
				{props.title}
			</ModalHeader>
			<ModalBody {...(props.modalBodyProps || {})}>{props.message}</ModalBody>
			<ModalFooter {...(props.modalFooterProps || {})}>
				{props.confirmText && (
					<Button
						color={props.confirmColor}
						onClick={() => onResolve(true)}
					>
						{props.confirmText}
					</Button>
				)}
				{props.cancelText && (
					<Button
						color={props.cancelColor}
						onClick={() => onResolve(false)}
					>
						{props.cancelText}
					</Button>
				)}
			</ModalFooter>
		</Modal>
	);
}