import React from 'react';
import ReactDOM from 'react-dom';
import isString from 'lodash/isString';

import locale from './locale/ru-RU';
import { PromptModal } from './PromptModal';
import { BaseModal } from './BaseModal';

const DESTROY_TIMEOUT = 600;


function parseArgs(obj) {
	let params = {
		...locale
	};
	if(obj.locale) {
		try {
			const loadedLocale = require(`./locale/${obj.locale}`);
			params = {
				...params,
				...loadedLocale.default
			}
		} catch (e) {
			throw new Error(`Locale ${obj.locale} not found`)
		}
	}
	if (isString(obj)) {
		params.message = obj;
	} else {
		params = { ...params, ...obj };
	}
	return params;
}

async function BaseRender(params,ReactEl) {
	const element = document.body.appendChild(document.createElement('div'));

	return new Promise((done, reject) => {
		try {
			const onResolve = value => {
				done(value);
				setTimeout(() => {
					ReactDOM.unmountComponentAtNode(element);
					element.remove();
				}, DESTROY_TIMEOUT);
			};
			ReactDOM.render(
				<ReactEl {...params} resolve={onResolve} />,
				element
			);
		} catch (e) {
			reject(e);
		}
	});
}

/**
 *
 * @param {string} obj.locale
 * @param {string} obj.title
 * @param {string} obj.message
 *
 * @param obj.modalProps {object} Props for <Modal/>
 * @param obj.modalHeaderProps {object} Props for <ModalHeader/>
 * @param obj.modalBodyProps {object} Props for <ModalBody/>
 * @param obj.modalFooterProps {object} Props for <ModalFooter/>
 *
 * @param obj.confirmColor {string} color for confirm button (see https://reactstrap.github.io/components/buttons/)
 * @param obj.cancelColor {string} color for confirm button (see https://reactstrap.github.io/components/buttons/)
 *
 * @param obj.confirmText {string} text for confirm button - The standard value is created from the localization
 * @param obj.cancelText {string} text for cancel button - The standard value is created from the localization
 * @returns {Promise<boolean>}
 */
export async function Confirm(obj) {
	const params = parseArgs(obj);
	return BaseRender(params,BaseModal);
}

/**
 *
 * @param {string} obj.locale
 * @param {string} obj.title
 * @param {string} obj.message
 *
 * @param obj.modalProps {object} Props for <Modal/>
 * @param obj.modalHeaderProps {object} Props for <ModalHeader/>
 * @param obj.modalBodyProps {object} Props for <ModalBody/>
 * @param obj.modalFooterProps {object} Props for <ModalFooter/>
 *
 * @param obj.confirmColor {string} color for confirm button (see https://reactstrap.github.io/components/buttons/)
 *
 * @param obj.confirmText {string} text for confirm button - The standard value is created from the localization
 * @returns {Promise<boolean>} always return true
 */
export function Alert(obj) {
	const params = parseArgs(obj);
	params.cancelText = null;
	return Confirm(params);
}

/**
 * @function
 * @param {string} obj.locale
 * @param {string} obj.title
 * @param {string} obj.message
 *
 * @param obj.modalProps {object} Props for <Modal/>
 * @param obj.modalHeaderProps {object} Props for <ModalHeader/>
 * @param obj.modalBodyProps {object} Props for <ModalBody/>
 * @param obj.modalFooterProps {object} Props for <ModalFooter/>
 * @param obj.inputProps {object} Props for <Input/>
 *
 * @param obj.confirmColor {string} color for confirm button (see https://reactstrap.github.io/components/buttons/)
 * @param obj.cancelColor {string} color for confirm button (see https://reactstrap.github.io/components/buttons/)
 *
 * @param obj.confirmText {string} text for confirm button - The standard value is created from the localization
 * @param obj.cancelText {string} text for cancel button - The standard value is created from the localization
 * @returns {Promise<string>}
 */
export function Prompt(obj) {
	const params = parseArgs(obj);
	return BaseRender(params,PromptModal);
}

export {
	Alert as AlertModal,
	Confirm as ConfirmModal,
	Prompt as PromptModal
}
