import React, {useState} from 'react';
import { Button, Input, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

export function PromptModal(props) {
	const [isOpen, _toggle] = useState(true);
	const [value, setValue] = useState(()=> props.defaultValue || '');

	function toggle() {
		_toggle(false);
		props.resolve(false);
	}

	function onResolve() {
		_toggle(false);
		props.resolve(value);
	}

	return (
		<Modal isOpen={isOpen} toggle={toggle} {...(props.modalProps || {})}>
			<ModalHeader toggle={toggle} {...(props.modalHeaderProps || {})}>{props.title}</ModalHeader>
			<ModalBody {...(props.modalBodyProps || {})}>
				{props.message}
				<Input
					value={value}
					onChange={e => setValue(e.target.value)}
					{...props.inputProps || {}}
					onKeyDown={e=> e.keyCode === 13 ? onResolve() : false}
				/>
			</ModalBody>
			<ModalFooter
				{...(props.modalFooterProps || {})}
			>
				<Button
					disabled={!value}
					className={props.confirmColor || 'primary'}
					onClick={onResolve}
					{...(props.confirmButtonProps || {})}
				>
					{props.confirmText || 'Продолжить'}
				</Button>
			</ModalFooter>
		</Modal>
	);
}