const locale = {
	title: 'Warning',
	message: '',
	confirmText: 'ok',
	confirmColor: 'primary',
	cancelText: 'cancel'
};

export default locale;