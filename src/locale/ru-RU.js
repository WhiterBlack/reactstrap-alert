const locale = {
	title: 'Внимание',
	message: '',
	confirmText: 'Да',
	confirmColor: 'primary',
	cancelText: 'Нет'
};

export default locale;